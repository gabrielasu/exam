#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <err.h>
#include <string.h>
#include <time.h>
#include "threadgroup.h"

int groupno;
int groupsactive;
pthread_mutex_t mutex;
pthread_cond_t cond;

void set_groupno(struct grouphead *head);
void reset_and_die(struct thread_params *params);
void *thread_func(void *thr_params) {
	struct thread_params *params = (struct thread_params *)thr_params;
	printf("%d/%d started.\n", params->entry->groupno, params->threadno);

	reset_and_die(params);

	free(params);
	return (NULL);
}

void start_thread(pthread_t *thread, struct grouphead *h, struct groupentry *entr) {
	struct thread_params *arg = malloc(sizeof (struct thread_params));
	if (arg == NULL) {
		err(1, "malloc");
	}

	arg->entry = entr;
	arg->groups = h;
	arg->threadno = entr->threadcount++;
	
	int er;
	if ((er = pthread_create(thread, NULL, thread_func, arg)) != 0) {
		errx(1, "pthread_create: %s", strerror(er));
	}

};

void print_remaining(struct grouphead *h) {
	printf("teams");
	struct groupentry *en = STAILQ_FIRST(h);

	while (en != NULL) {
		printf(" %d:%d", en->groupno, en->threadcount);
		en = STAILQ_NEXT(en, entries);
	}

	printf("\n");
};

void reset_and_die(struct thread_params *params) {
	pthread_mutex_lock(&mutex);

	while (params->entry->groupno != groupno) {
		pthread_cond_wait(&cond, &mutex);
	}

	printf("== %d/%d thread picked ==\n", params->entry->groupno, params->threadno);

	params->entry->threadcount--;

	printf("%d/%d remaining threads now %d.\n", params->entry->groupno, \
		params->threadno, params->entry->threadcount);

	if (params->entry->threadcount == 0) {
		STAILQ_REMOVE(params->groups, params->entry, groupentry, entries);
		groupsactive--;
	}

	if (groupsactive) {
		set_groupno(params->groups);

		printf("%d/%d chose next team %d.\n", params->entry->groupno, \
			params->threadno, groupno);

		print_remaining(params->groups);
		pthread_cond_broadcast(&cond);
	}

	if (params->entry->threadcount == 0) {
		free(params->entry);
	}

	pthread_mutex_unlock(&mutex);
};

void set_groupno(struct grouphead *head) {
	int no = rand() % groupsactive;
	
	struct groupentry *entry = STAILQ_FIRST(head);
	for (int i = 0; i < no; i++) {
		entry = STAILQ_NEXT(entry, entries);
	}

	groupno = entry->groupno;
};

void init() {
	/* mutex init */
	pthread_mutexattr_t attr;

	(void) pthread_mutexattr_init(&attr);

	if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK) != 0) {
		err(1, "mutexattr_settype");
	}

	if (pthread_mutex_init(&mutex, &attr) == -1) {
		err(1, "mutex_init");
	}

	/* cond var init */
	if (pthread_cond_init(&cond, NULL) == -1) {
		err(1, "cond_init");
	}

	srand(time(NULL));
};

int run_groups(int groupcount, int threadcount) {
	/* init */
	init();

	pthread_t *t_ids = malloc(sizeof (pthread_t) * groupcount * threadcount);
	if (t_ids == NULL) {
		err(1, "malloc");
	}

	groupno = -1;
	groupsactive = groupcount;
	struct grouphead ghead = STAILQ_HEAD_INITIALIZER(ghead);

	/* starting threads */
	for (int i = 0; i < groupcount; i++) {
		struct groupentry *entry = malloc(sizeof (struct groupentry));
		if (entry == NULL) {
			err(1, "malloc");
		}
		
		entry->groupno = i;
		entry->threadcount = 0;

		STAILQ_INSERT_HEAD(&ghead, entry, entries);
		
		for (int j = 0; j < threadcount; j++) {
			start_thread(&t_ids[i * threadcount + j], &ghead, entry);
		}
	}

	/* initial cond set */
	pthread_mutex_lock(&mutex);

	set_groupno(&ghead);
	printf("main picking team %d.\n", groupno);
		
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);

	/* join */
	for (int i = 0; i < (groupcount * threadcount); i++) {
		int er;
		if ((er = pthread_join(t_ids[i], NULL)) != 0) {
			errx(1, "pthread_join: %s", strerror(er));
		}
	}

	printf("main woke up.\n");
	/* cleanup  */
	free(t_ids);
	pthread_cond_destroy(&cond);
	pthread_mutex_destroy(&mutex);

	if (!STAILQ_EMPTY(&ghead)) {
		printf("stailq");
	}

	return (0);
};
