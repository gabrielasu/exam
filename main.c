#include <stdio.h>
#include <stdlib.h>
#include "threadgroup.h"

int main(int argc, char **argv)
{
	if (argc != 3) {
		printf("usage: thrgroup <n> <m>");
	}

	int groupcount = atoi(argv[1]);
	int threadcount = atoi(argv[2]);

	run_groups(groupcount, threadcount);
	return (0);
}
