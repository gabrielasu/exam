#include <sys/queue.h>

STAILQ_HEAD(grouphead, groupentry);

struct groupentry {
	STAILQ_ENTRY(groupentry) entries;

	int groupno;
	int threadcount;
};

struct thread_params {
	int threadno;

	struct groupentry *entry;
	struct grouphead *groups;
};

int run_groups(int groupcount, int threadcount);
