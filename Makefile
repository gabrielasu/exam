# only a wrapper Makefile
.PHONY = all clean

all: thrgroup

thrgroup:
	gcc -Wall -Wextra -o thrgroup threadgroup.c main.c -lpthread

clean:
	rm *.o thrgroup
